<?php

use Illuminate\Http\Request;
use app\Estado;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/json', function () {
    return "Ola Json API";
});

Route::get('/cep', function () {

    $dados = array(
        "cep" => "85660000",
        "cidade" => "Dois Vizinhos",
        "uf" => "PR"
    );

    return $dados;

});



Route::get('/funcionarios', function () {
    $funcionarios = [
        array(
            "nome" => "Joao da silva",
            "idade" => 12
        ),
        array(
            "nome" => "Maria de Oliveira",
            "idade" => 20
        )
    ];

    $dados = array(
        "funcionarios" => $funcionarios,
        "qtd" => count($funcionarios)
    );

    return response()->json( $dados );

});

//array de resources
Route::resources([
    '/Estados' => 'EstadoController',
    '/Cidades' => 'CidadeController',
    '/EstadosDB' => 'EstadoDBController',
    '/CidadesDB' => 'CidadeDBController'
]);

// Route::resource('/Estados', 'EstadoController');
// Route::resource('/Cidades', 'CidadeController');
// Route::resource('/EstadosDB', 'EstadoDBController');
// Route::resource('/CidadesDB', 'CidadeDBController');

//'teste' é o metodo criado na Estado controller
//'CI/Cidades' são os parametros para chamar o metodo novo  
Route::get('Estados/CI/Cidades', 'EstadoController@teste');


//Route::post('/testepost',function (){ return response()->json(["msg"=>'Request Post']); } );
//Route::delete('/testedelete',function (){ return response()->json(["msg"=>'Request Delete']); } );


