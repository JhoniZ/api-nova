<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estado;


class EstadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // return Estado::all();

        //exemplo de paginacao
        //padrao é 15

        // return Estado::paginate(2);
        $qtd = $request->input('qtd');
        // return Estado::paginate($qtd);
        return Estado::with('cidadeFuncao')->paginate($qtd);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */



    //Inserir registro na tabela de estado
    public function store(Request $request)
    {
        //Inserir registro na tebela estado
        $estado = new Estado();

        //$estado->descricao = $request->input('descricao');
        //$estado->uf = $request->input('uf');

        $estado->fill($request->all());

        $estado->save();

        return $estado;
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $estado = Estado::find($id);
        // return $estado;

        $estado = Estado::with('cidadeFuncao')->find($id);

        return $estado;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $estado = Estado::find($id);

        $estado->fill( $request->all() );

        $estado->save();

        return $estado;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $estado = Estado::find($id);

        $estado->delete();

        return $estado;

    }

    public function teste()
    {
        return "Metodo teste funcionou";
    }

}
