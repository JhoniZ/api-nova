<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $fillable = ['descricao', 'uf'];


    public function cidadeFuncao(){
    	return $this->hasMany('App\Cidade', 'id_estado', 'id');
    }
}
