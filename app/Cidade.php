<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    protected $fillable = ['cidade','id_estado']; 

    public function estadoFuncao(){

    	return $this->belongsTo('App\Estado', 'id_estado', 'id');

    }
}
